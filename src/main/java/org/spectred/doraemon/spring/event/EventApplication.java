package org.spectred.doraemon.spring.event;

import org.spectred.doraemon.spring.event.event.NormalEvent;
import org.spectred.doraemon.spring.event.service.NormalServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class EventApplication {

    public static void main(String[] args) {
        System.out.println("准备初始化IOC容器。。。");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("org.spectred.doraemon.spring.event");
        System.out.println("IOC容器初始化完成。。。");

        // 1. 主动发布事件
        context.publishEvent(new NormalEvent("Hi Java"));

        // 2. 调用相应方法后触发事件
        NormalServiceImpl normalService = context.getBean(NormalServiceImpl.class);
        normalService.normal("hello world");

        context.close();
        System.out.println("IoC容器关闭...");
    }
}
