package org.spectred.doraemon.spring.event;

import org.spectred.doraemon.spring.event.event.HierarchicalEvent;
import org.spectred.doraemon.spring.event.listener.HierarchicalEventListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class HierarchicalEventApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext parentContext = new AnnotationConfigApplicationContext();
        parentContext.addApplicationListener(new HierarchicalEventListener());

        AnnotationConfigApplicationContext childContext = new AnnotationConfigApplicationContext();
        childContext.setParent(parentContext);
        childContext.addApplicationListener(new HierarchicalEventListener());

        parentContext.refresh();
        childContext.refresh();

        parentContext.publishEvent(new HierarchicalEvent("父容器事件"));
        childContext.publishEvent(new HierarchicalEvent("子容器事件"));
    }
}
