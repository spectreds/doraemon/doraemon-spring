package org.spectred.doraemon.spring.event.listener;

import org.spectred.doraemon.spring.event.event.NormalEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@EnableAsync
@Order(0)
@Component
public class NormalListenerA {

    @Async
    @EventListener
    public void onNormalEvent(NormalEvent normalEvent) {
        System.out.println("NormalListenerA Running ..." + normalEvent.getSource());
        System.out.println(Thread.currentThread().getName());
    }
}
