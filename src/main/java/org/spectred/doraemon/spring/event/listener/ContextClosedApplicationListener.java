package org.spectred.doraemon.spring.event.listener;

import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
public class ContextClosedApplicationListener {

    @EventListener
    public void onContextClosedEvent(ContextClosedEvent event){
        System.out.println("ContextClosedApplicationListener Running...");
    }
}
