package org.spectred.doraemon.spring.event.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author spectred
 */
public class NormalEvent extends ApplicationEvent {

    private String name;

    public NormalEvent(String name) {
        super(name);
    }


}
