package org.spectred.doraemon.spring.event.service;

import org.spectred.doraemon.spring.event.event.NormalEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
public class NormalServiceImpl implements ApplicationEventPublisherAware {

    ApplicationEventPublisher publisher;

    public void normal(String string) {
        publisher.publishEvent(new NormalEvent(string));
    }


    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
