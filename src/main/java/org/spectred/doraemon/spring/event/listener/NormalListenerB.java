package org.spectred.doraemon.spring.event.listener;

import org.spectred.doraemon.spring.event.event.NormalEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@EnableAsync
@Order(1)
@Component
public class NormalListenerB implements ApplicationListener<NormalEvent> {

    @Async
    @Override
    public void onApplicationEvent(NormalEvent event) {
        System.out.println("NormalListenerB Running ..." + event.getSource());
        System.out.println(Thread.currentThread().getName());
    }
}
