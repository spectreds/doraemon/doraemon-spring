package org.spectred.doraemon.spring.event.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author spectred
 */
public class HierarchicalEvent extends ApplicationEvent {

    public HierarchicalEvent(Object source) {
        super(source);
    }
}
