package org.spectred.doraemon.spring.postprocessor.a_quickstart;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class QuickStartApplication {

    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("org.spectred.doraemon.spring.postprocessor.a_quickstart");
        context.close();
    }
}
