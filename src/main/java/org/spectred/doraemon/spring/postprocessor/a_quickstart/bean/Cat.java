package org.spectred.doraemon.spring.postprocessor.a_quickstart.bean;

import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
public class Cat {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                '}';
    }
}
