package org.spectred.doraemon.spring.annotation;

import org.spectred.doraemon.spring.annotation.bean.Cat;
import org.spectred.doraemon.spring.annotation.config.ComponentScanConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * @author spectred
 */
public class ComponentScanApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ComponentScanConfiguration.class);

        Cat cat = applicationContext.getBean(Cat.class);
        System.out.println(cat);

        Arrays.stream(applicationContext.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
