package org.spectred.doraemon.spring.annotation;

import org.spectred.doraemon.spring.annotation.bean.Cat;
import org.spectred.doraemon.spring.annotation.bean.Person;
import org.spectred.doraemon.spring.annotation.config.ComponentScanConfiguration;
import org.spectred.doraemon.spring.annotation.config.QuickstartConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * @author spectred
 */
public class AnnotationApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(QuickstartConfiguration.class);

        Person person = context.getBean(Person.class);
        System.out.println(person);
    }
}
