package org.spectred.doraemon.spring.annotation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@ComponentScan( "org.spectred.doraemon.spring.annotation.bean")
@Configuration
public class ComponentScanConfiguration {
}
