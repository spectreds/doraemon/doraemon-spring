package org.spectred.doraemon.spring.annotation.config;

import org.spectred.doraemon.spring.annotation.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class QuickstartConfiguration {

    @Bean("person")
    public Person person() {
        return new Person();
    }
}
