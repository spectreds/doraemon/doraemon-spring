package org.spectred.doraemon.spring.basic_di.b_constructor.config;

import org.spectred.doraemon.spring.basic_di.b_constructor.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class ConstructorConfiguration {

    @Bean("jack")
    public Person person() {
        return new Person("Jack", 18);
    }
}
