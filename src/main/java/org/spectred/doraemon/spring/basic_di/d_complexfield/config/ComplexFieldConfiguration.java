package org.spectred.doraemon.spring.basic_di.d_complexfield.config;

import org.spectred.doraemon.spring.basic_di.d_complexfield.bean.Cat;
import org.spectred.doraemon.spring.basic_di.d_complexfield.bean.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author spectred
 */
@Configuration
@ComponentScan("org.spectred.doraemon.spring.basic_di.d_complexfield.bean")
public class ComplexFieldConfiguration {

    @Bean("master")
    public Person master() {
        Person person = new Person();
        person.setName("master");
        return person;
    }

    @Primary
    @Bean("admin")
    public Person admin() {
        Person person = new Person();
        person.setName("admin");
        return person;
    }

    @Bean
    public Cat cat(@Qualifier("admin") Person person) {
        Cat cat = new Cat();
        cat.setName("cat");
        cat.setPerson(person);
        return new Cat();
    }
}
