package org.spectred.doraemon.spring.basic_di.b_constructor.bean;

import lombok.AllArgsConstructor;

/**
 * @author spectred
 */
@AllArgsConstructor
public class Person {

    private String name;

    private Integer age;
}
