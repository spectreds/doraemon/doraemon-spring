package org.spectred.doraemon.spring.basic_di.c_value_spel;

import org.spectred.doraemon.spring.basic_di.c_value_spel.bean.Black;
import org.spectred.doraemon.spring.basic_di.c_value_spel.bean.Red;
import org.spectred.doraemon.spring.basic_di.c_value_spel.config.InjectValueConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class InjectValueAnnoApplication {

    public static void main(String[] args){
        ApplicationContext context = new AnnotationConfigApplicationContext("org.spectred.doraemon.spring.basic_di.c_value_spel.bean");
        Black black = context.getBean(Black.class);
        System.out.println(black);


        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(InjectValueConfiguration.class);
        Red red = applicationContext.getBean(Red.class);
        System.out.println(red);
    }
}
