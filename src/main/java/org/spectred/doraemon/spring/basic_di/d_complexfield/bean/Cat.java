package org.spectred.doraemon.spring.basic_di.d_complexfield.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Data
@Component
public class Cat {

    private String name;

    private Person person;

}
