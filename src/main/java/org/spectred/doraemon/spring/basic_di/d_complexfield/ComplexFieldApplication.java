package org.spectred.doraemon.spring.basic_di.d_complexfield;

import org.spectred.doraemon.spring.basic_di.d_complexfield.bean.Cat;
import org.spectred.doraemon.spring.basic_di.d_complexfield.bean.Dog;
import org.spectred.doraemon.spring.basic_di.d_complexfield.config.ComplexFieldConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class ComplexFieldApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("org.spectred.doraemon.spring.basic_di.d_complexfield.bean");
        Dog dog = context.getBean(Dog.class);
        System.out.println(dog);

        ApplicationContext ctx = new AnnotationConfigApplicationContext(ComplexFieldConfiguration.class);
        Cat cat = ctx.getBean(Cat.class);
        System.out.println(cat);
    }
}
