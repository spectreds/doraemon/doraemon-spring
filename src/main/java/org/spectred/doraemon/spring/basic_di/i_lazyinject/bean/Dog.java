package org.spectred.doraemon.spring.basic_di.i_lazyinject.bean;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
public class Dog {

    private Person person;

    @Autowired
    public void setPerson(ObjectProvider<Person> person) {
        // 延迟setter注入
        this.person = person.getIfAvailable();
    }

//    @Autowired
//    public Dog(ObjectProvider<Person> person) {
//        // 延迟构造注入
//        this.person = person.getIfAvailable();
//    }
}
