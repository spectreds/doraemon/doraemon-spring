package org.spectred.doraemon.spring.basic_di.a_quickstart_set.config;

import org.spectred.doraemon.spring.basic_di.a_quickstart_set.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class QuickStartSetConfiguration {

    @Bean
    public Person person() {
        Person person = new Person();
        person.setName("Jack");
        person.setAge(18);
        return person;
    }
}
