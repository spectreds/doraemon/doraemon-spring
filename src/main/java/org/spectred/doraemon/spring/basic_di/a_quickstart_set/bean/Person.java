package org.spectred.doraemon.spring.basic_di.a_quickstart_set.bean;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Person {

    private String name;

    private Integer age;
}
