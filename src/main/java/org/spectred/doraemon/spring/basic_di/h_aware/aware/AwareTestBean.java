package org.spectred.doraemon.spring.basic_di.h_aware.aware;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Arrays;

/**
 * @author spectred
 */
public class AwareTestBean implements ApplicationContextAware {

    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public ApplicationContext getContext() {
        return context;
    }

    public void printBeanNames() {
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
