package org.spectred.doraemon.spring.basic_di.d_complexfield.bean;

import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@ToString
@Setter
@Component
public class Dog {

    @Value("dog-name")
    private String name;

    @Qualifier("person-A")
    @Autowired
    private Person person;
}
