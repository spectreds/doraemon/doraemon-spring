package org.spectred.doraemon.spring.basic_di.h_aware;

import org.spectred.doraemon.spring.basic_di.h_aware.aware.AwareTestBean;
import org.spectred.doraemon.spring.basic_di.h_aware.config.AwareConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class AwareApplication {

    public static void main(String[] args){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AwareConfiguration.class);
        AwareTestBean awareTestBean = ctx.getBean(AwareTestBean.class);
        awareTestBean.printBeanNames();
    }
}
