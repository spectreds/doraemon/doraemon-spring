package org.spectred.doraemon.spring.basic_di.c_value_spel.bean;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@ToString
@Component
public class Red {

    @Value("${red.name}")
    private String name;

    @Value("${red.attr}")
    private String attr;
}
