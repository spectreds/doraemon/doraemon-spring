package org.spectred.doraemon.spring.basic_di.z_inject_set;

import org.spectred.doraemon.spring.basic_di.z_inject_set.bean.Cat;
import org.spectred.doraemon.spring.basic_di.z_inject_set.bean.Person;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author spectred
 */
public class QuickStartInjectBySetXmlApplication {

    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("basic_di/inject-set.xml");
        ;
        Person person = beanFactory.getBean(Person.class);
        System.out.println(person);

        Cat cat = beanFactory.getBean(Cat.class);
        System.out.println(cat);
    }
}
