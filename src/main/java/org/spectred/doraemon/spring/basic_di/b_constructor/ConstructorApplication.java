package org.spectred.doraemon.spring.basic_di.b_constructor;

import org.spectred.doraemon.spring.basic_di.b_constructor.config.ConstructorConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

/**
 * @author spectred
 */
public class ConstructorApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConstructorConfiguration.class);
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);

        System.out.println("---");

        ApplicationContext xmlContext = new ClassPathXmlApplicationContext("basic_di/basic_di.xml");
        Arrays.stream(xmlContext.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
