package org.spectred.doraemon.spring.basic_di.d_complexfield.bean;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Data
@Component("person-A")
public class Person {

    private String name = "administrator";
}
