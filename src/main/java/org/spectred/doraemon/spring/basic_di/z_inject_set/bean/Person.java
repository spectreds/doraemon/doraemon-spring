package org.spectred.doraemon.spring.basic_di.z_inject_set.bean;


import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Person {

    private String name;

    private Integer age;
}
