package org.spectred.doraemon.spring.basic_di.h_aware.config;

import org.spectred.doraemon.spring.basic_di.h_aware.aware.AwareTestBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class AwareConfiguration {

    @Bean
    public AwareTestBean awareTestBean() {
        return new AwareTestBean();
    }
}
