package org.spectred.doraemon.spring.basic_dl.d_withanno.bean;

import org.spectred.doraemon.spring.basic_dl.d_withanno.anno.Color;

/**
 * @author spectred
 */
@Color
public class Green {
}
