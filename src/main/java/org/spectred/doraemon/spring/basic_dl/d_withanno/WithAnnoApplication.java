package org.spectred.doraemon.spring.basic_dl.d_withanno;

import org.spectred.doraemon.spring.basic_dl.d_withanno.anno.Color;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;
import java.util.stream.Stream;

/**
 * @author spectred
 */
public class WithAnnoApplication {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("basic_dl/quickstart-withanno.xml");

        Map<String, Object> beanMap = context.getBeansWithAnnotation(Color.class);
        beanMap.forEach((beanName, bean) -> System.out.println(beanName + ":\t" + bean));

        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Stream.of(beanDefinitionNames).forEach(System.out::println);
    }
}
