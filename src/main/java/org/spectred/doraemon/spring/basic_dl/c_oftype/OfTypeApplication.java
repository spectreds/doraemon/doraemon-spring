package org.spectred.doraemon.spring.basic_dl.c_oftype;

import org.spectred.doraemon.spring.basic_dl.c_oftype.dao.DemoDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

/**
 * @author spectred
 */
public class OfTypeApplication {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("basic_dl/quickstart-oftype.xml");

        Map<String, DemoDao> beansMap = context.getBeansOfType(DemoDao.class);

        beansMap.forEach((beanName, bean) -> System.out.println(beanName + "\t:\t" + bean));
    }
}
