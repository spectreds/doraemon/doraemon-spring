package org.spectred.doraemon.spring.basic_dl.b_bytype;

import org.spectred.doraemon.spring.basic_dl.b_bytype.bean.Person;
import org.spectred.doraemon.spring.basic_dl.b_bytype.dao.DemoDao;
import org.spectred.doraemon.spring.basic_dl.b_bytype.dao.impl.DemoDaoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author spectred
 */
public class ByTypeApplication {

    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("basic_dl/quickstart-bytype.xml");
        Person person = beanFactory.getBean(Person.class);
        System.out.println(person);

        DemoDao demoDao = beanFactory.getBean(DemoDaoImpl.class);
        List<String> all = demoDao.findAll();
        System.out.println(all);
    }
}
