package org.spectred.doraemon.spring.basic_dl.a_byname;

import org.spectred.doraemon.spring.basic_dl.a_byname.bean.Person;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author spectred
 */
public class QuickStartByNameApplication {

    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("basic_dl/quickstart-byname.xml");
        Person person = (Person) beanFactory.getBean("person");
        System.out.println(person);
    }
}
