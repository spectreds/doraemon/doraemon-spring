package org.spectred.doraemon.spring.basic_dl.e_lazylookup;

import org.spectred.doraemon.spring.basic_dl.e_lazylookup.bean.Dog;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author spectred
 */
public class LazyLookupApplication {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("basic_dl/quickstart-lazylookup.xml");

        ObjectProvider<Dog> beanProvider = context.getBeanProvider(Dog.class);
        Dog dog = beanProvider.getIfAvailable(Dog::new);
        System.out.println(dog);

    }
}
