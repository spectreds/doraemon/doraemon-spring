package org.spectred.doraemon.spring.basic_dl.b_bytype.dao.impl;

import org.spectred.doraemon.spring.basic_dl.b_bytype.dao.DemoDao;

import java.util.List;

/**
 * @author spectred
 */
public class DemoDaoImpl implements DemoDao {

    @Override
    public List<String> findAll() {
        return List.of("a", "b", "c");
    }
}
