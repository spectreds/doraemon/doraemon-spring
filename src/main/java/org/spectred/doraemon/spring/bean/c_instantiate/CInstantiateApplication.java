package org.spectred.doraemon.spring.bean.c_instantiate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;


/**
 * @author spectred
 */
public class CInstantiateApplication {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean/staticfactory.xml");
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}