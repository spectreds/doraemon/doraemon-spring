package org.spectred.doraemon.spring.bean.a_type.bean;

/**
 * @author spectred
 */
public class Car extends Toy{

    public Car(String name) {
        super(name);
    }
}
