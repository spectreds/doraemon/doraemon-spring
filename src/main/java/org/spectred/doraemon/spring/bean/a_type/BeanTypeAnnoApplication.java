package org.spectred.doraemon.spring.bean.a_type;

import org.spectred.doraemon.spring.bean.a_type.bean.Toy;
import org.spectred.doraemon.spring.bean.a_type.config.ATypeConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class BeanTypeAnnoApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ATypeConfiguration.class);
        Toy toy = context.getBean(Toy.class);
        System.out.println(toy);

        System.out.println(context.getBean("toyFactoryBean"));

        System.out.println(context.getBean("&toyFactoryBean"));

    }
}
