package org.spectred.doraemon.spring.bean.c_instantiate.bean;

/**
 * @author spectred
 */
public class Car {

    public Car() {
        System.out.println(Car.class.getName()+": constructor run");
    }
}
