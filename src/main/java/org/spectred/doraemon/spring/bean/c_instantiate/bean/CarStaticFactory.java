package org.spectred.doraemon.spring.bean.c_instantiate.bean;

/**
 * @author spectred
 */
public class CarStaticFactory {

    public static Car getCar(){
        return new Car();
    }
}
