package org.spectred.doraemon.spring.bean.a_type.bean;

import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;

/**
 * @author spectred
 */
public class ToyFactoryBean implements FactoryBean<Toy> {

    @Setter
    private Child child;


    @Override
    public Toy getObject() throws Exception {
        Toy toy;
        switch (child.getWantToy()) {
            case "ball":
                toy = new Ball("ball");
                break;
            case "car":
                toy = new Car("car");
                break;
            default:
                toy = null;
                break;
        }
        return toy;
    }

    @Override
    public Class<?> getObjectType() {
        return Toy.class;
    }
}
