package org.spectred.doraemon.spring.bean.b_scope.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Pet {

    public Pet() {
        System.out.println("Pet constructor run ...");
    }
}
