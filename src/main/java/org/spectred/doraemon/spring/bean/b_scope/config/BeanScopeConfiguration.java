package org.spectred.doraemon.spring.bean.b_scope.config;

import org.spectred.doraemon.spring.bean.b_scope.bean.Child;
import org.spectred.doraemon.spring.bean.b_scope.bean.Pet;
import org.spectred.doraemon.spring.bean.b_scope.bean.Toy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author spectred
 */
@Configuration
@ComponentScan("org.spectred.doraemon.spring.bean.b_scope.bean")
public class BeanScopeConfiguration {

    @Bean
    public Child child1(Toy toy, Pet pet) {
        Child child = new Child();
        child.setToy(toy);
        child.setPet(pet);
        return child;
    }

    @Bean
    public Child child2(Toy toy, Pet pet) {
        Child child = new Child();
        child.setToy(toy);
        child.setPet(pet);
        return child;
    }
}
