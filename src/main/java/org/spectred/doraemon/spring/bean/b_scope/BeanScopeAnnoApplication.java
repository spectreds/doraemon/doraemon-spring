package org.spectred.doraemon.spring.bean.b_scope;

import org.spectred.doraemon.spring.bean.b_scope.bean.Child;
import org.spectred.doraemon.spring.bean.b_scope.config.BeanScopeConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * @author spectred
 */
public class BeanScopeAnnoApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanScopeConfiguration.class);
        context.getBeansOfType(Child.class).forEach((name, child) -> System.out.println(name + ":\t" + child.getToy() + " \t" + child.getPet()));
    }
}
