package org.spectred.doraemon.spring.bean.a_type.bean;

/**
 * @author spectred
 */
public class Ball extends Toy {

    public Ball(String name) {
        super(name);
    }
}
