package org.spectred.doraemon.spring.bean.b_scope.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * @author spectred
 */
public class Child {

    @Setter
    @Getter
    private Toy toy;

    @Setter
    @Getter
    private Pet pet;
}
