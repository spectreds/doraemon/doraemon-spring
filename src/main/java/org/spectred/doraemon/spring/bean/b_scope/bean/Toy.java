package org.spectred.doraemon.spring.bean.b_scope.bean;

import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Component
public class Toy {

    public Toy() {
        System.out.println("Toy constructor run ...");
    }
}
