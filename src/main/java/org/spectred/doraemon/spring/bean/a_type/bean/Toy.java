package org.spectred.doraemon.spring.bean.a_type.bean;

/**
 * @author spectred
 */
public abstract class Toy {

    private String name;

    public Toy(String name) {
        this.name = name;
    }
}
