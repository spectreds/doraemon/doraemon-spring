package org.spectred.doraemon.spring.lifecycle.b_jsr250.config;

import org.spectred.doraemon.spring.lifecycle.b_jsr250.bean.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class BJsr250Configuration {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public Dog dog() {
        Dog dog = new Dog();
        dog.setName("dog-name");
        return dog;
    }
}
