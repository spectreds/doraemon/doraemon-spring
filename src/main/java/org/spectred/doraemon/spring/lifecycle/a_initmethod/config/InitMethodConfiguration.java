package org.spectred.doraemon.spring.lifecycle.a_initmethod.config;

import org.spectred.doraemon.spring.lifecycle.a_initmethod.bean.Cat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class InitMethodConfiguration {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public Cat cat() {
        Cat cat = new Cat("cat");
        cat.setName("a cat");
        return cat;
    }
}
