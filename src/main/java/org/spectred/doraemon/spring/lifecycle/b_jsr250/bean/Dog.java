package org.spectred.doraemon.spring.lifecycle.b_jsr250.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author spectred
 */
public class Dog {

    private String name;

    public Dog() {
        System.out.println(this.getClass().getSimpleName() + ":Constructor running...1");
    }

    public void setName(String name) {
        System.out.println(this.getClass().getSimpleName() + ":Setter running...2");
        this.name = name;
    }

    public void init(){
        System.out.println(this.getClass().getSimpleName() + ":Init running...4");
    }

    public void destroy(){
        System.out.println(this.getClass().getSimpleName() + ":Destroy running...6");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println(this.getClass().getSimpleName() + ":postConstruct running...3");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println(this.getClass().getSimpleName() + ":preDestroy running...5");
    }

}
