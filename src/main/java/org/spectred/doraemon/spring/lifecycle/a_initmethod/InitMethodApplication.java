package org.spectred.doraemon.spring.lifecycle.a_initmethod;

import org.spectred.doraemon.spring.lifecycle.a_initmethod.bean.Cat;
import org.spectred.doraemon.spring.lifecycle.a_initmethod.config.InitMethodConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * @author spectred
 */
public class InitMethodApplication {

    public static void main(String[] args) {
        System.out.println("准备初始化IOC容器");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(InitMethodConfiguration.class);
        System.out.println("IOC容器初始化完成");

        System.out.println("准备销毁IOC容器");
        context.close();
        System.out.println("IOC容器销毁完成");

    }
}
