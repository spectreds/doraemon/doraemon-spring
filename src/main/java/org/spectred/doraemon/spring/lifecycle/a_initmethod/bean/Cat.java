package org.spectred.doraemon.spring.lifecycle.a_initmethod.bean;


/**
 * @author spectred
 */
public class Cat {

    private String name;

    public Cat(String name) {
        System.out.println(this.getClass().getSimpleName()+" constructor running...1");
        this.name = name;
    }

    public void setName(String name) {
        System.out.println(this.getClass().getSimpleName() + " setter running...2");
        this.name = name;
    }

    public void init() {
        System.out.println(this.getClass().getSimpleName() + " init method running...3");
    }

    public void destroy() {
        System.out.println(this.getClass().getSimpleName() + " destroy method running...4");
    }
}
