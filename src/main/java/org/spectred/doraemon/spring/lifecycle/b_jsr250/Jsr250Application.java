package org.spectred.doraemon.spring.lifecycle.b_jsr250;


import org.spectred.doraemon.spring.lifecycle.b_jsr250.config.BJsr250Configuration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class Jsr250Application {

    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BJsr250Configuration.class);

        context.close();
    }

}
