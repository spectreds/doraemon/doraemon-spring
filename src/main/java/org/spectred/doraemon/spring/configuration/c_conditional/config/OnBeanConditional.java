package org.spectred.doraemon.spring.configuration.c_conditional.config;

import org.spectred.doraemon.spring.configuration.c_conditional.annotation.ConditionalOnBean;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author spectred
 */
public class OnBeanConditional implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Map<String, Object> attributes = metadata.getAnnotationAttributes(ConditionalOnBean.class.getName());
        assert attributes != null;

        Class<?>[] classes = (Class<?>[]) attributes.get("value");
        boolean matchClazz = Stream.of(classes)
                .anyMatch(clazz -> Objects.requireNonNull(context.getBeanFactory()).containsBeanDefinition(clazz.getName()));
        if (matchClazz) {
            return true;
        }

        String[] beanNames = (String[]) attributes.get("beanNames");
        boolean matchBeanName = Stream.of(beanNames)
                .anyMatch(beanName -> Objects.requireNonNull(context.getBeanFactory()).containsBeanDefinition(beanName));
        if (matchBeanName) {
            return true;
        }
        return false;
    }
}
