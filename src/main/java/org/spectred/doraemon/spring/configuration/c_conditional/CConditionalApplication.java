package org.spectred.doraemon.spring.configuration.c_conditional;

import org.spectred.doraemon.spring.configuration.c_conditional.config.TavernConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author spectred
 */
public class CConditionalApplication {
    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(TavernConfiguration.class);
        context.refresh();

        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);

    }
}
