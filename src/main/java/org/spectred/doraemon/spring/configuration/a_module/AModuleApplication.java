package org.spectred.doraemon.spring.configuration.a_module;

import org.spectred.doraemon.spring.configuration.a_module.bean.Bar;
import org.spectred.doraemon.spring.configuration.a_module.bean.Bartender;
import org.spectred.doraemon.spring.configuration.a_module.bean.Boss;
import org.spectred.doraemon.spring.configuration.a_module.bean.Waiter;
import org.spectred.doraemon.spring.configuration.a_module.config.TavernConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;
import java.util.stream.Stream;

/**
 * @author spectred
 */
public class AModuleApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TavernConfiguration.class);
        Boss bean = context.getBean(Boss.class);
        System.out.println(bean);

        printLine("所有的bean");
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);

        printLine("Bartender");
        context.getBeansOfType(Bartender.class).forEach((k, v) -> System.out.println(k + ":" + v));

        printLine("Bar");
        context.getBeansOfType(Bar.class).forEach((k, v) -> System.out.println(k + ":" + v));

        printLine("Waiter");
       context.getBeansOfType(Waiter.class).forEach((k, v) -> System.out.println(k + ":" + v));
    }

    private static void printLine() {
        printLine("");
    }

    private static void printLine(String str) {
        System.out.println("------------" + str + "------------------");
    }
}
