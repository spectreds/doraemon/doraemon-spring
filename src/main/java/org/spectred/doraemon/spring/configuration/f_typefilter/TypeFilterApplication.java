package org.spectred.doraemon.spring.configuration.f_typefilter;

import org.spectred.doraemon.spring.configuration.f_typefilter.color.Red;
import org.spectred.doraemon.spring.configuration.f_typefilter.config.TypeFilterConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

/**
 * @author spectred
 */
public class TypeFilterApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TypeFilterConfig.class);
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);

        Red red = SpringClassHolder.getBean("red", Red.class);
        System.out.println(red);

    }
}
