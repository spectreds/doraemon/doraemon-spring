package org.spectred.doraemon.spring.configuration.c_conditional.config;

import org.spectred.doraemon.spring.configuration.c_conditional.bean.Bar;
import org.spectred.doraemon.spring.configuration.c_conditional.condition.ExistBossCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class BarConfiguration {

    @Conditional(ExistBossCondition.class)
    @Bean
    public Bar bar() {
        return new Bar();
    }


}
