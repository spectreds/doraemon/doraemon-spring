package org.spectred.doraemon.spring.configuration.a_module.config;

import org.spectred.doraemon.spring.configuration.a_module.bean.Bar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class BarConfiguration {

    @Bean
    public Bar bar(){
        return new Bar();
    }
}
