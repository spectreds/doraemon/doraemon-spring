package org.spectred.doraemon.spring.configuration.f_typefilter.config;

import org.spectred.doraemon.spring.configuration.f_typefilter.annotation.Animal;
import org.spectred.doraemon.spring.configuration.f_typefilter.color.Color;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

/**
 * @author spectred
 */
@Configuration
@ComponentScan(basePackages = "org.spectred.doraemon.spring.configuration.f_typefilter",
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Color.class),
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Animal.class),
                @ComponentScan.Filter(type = FilterType.CUSTOM, value = GreenTypeFilter.class)
        }
)
public class TypeFilterConfig {
}
