package org.spectred.doraemon.spring.configuration.b_profile.config;

import org.spectred.doraemon.spring.configuration.b_profile.bean.Bartender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author spectred
 */
@Configuration
@Profile("city")
public class BartenderConfiguration {

    @Bean
    public Bartender bartenderA() {
        Bartender bartender = new Bartender();
        bartender.setName("A");
        return bartender;
    }

    @Bean
    public Bartender bartenderB() {
        Bartender bartender = new Bartender();
        bartender.setName("B");
        return bartender;
    }
}
