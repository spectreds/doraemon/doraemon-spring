package org.spectred.doraemon.spring.configuration.c_conditional.config;

import org.spectred.doraemon.spring.configuration.c_conditional.annotation.ConditionalOnBean;
import org.spectred.doraemon.spring.configuration.c_conditional.bean.Bartender;
import org.spectred.doraemon.spring.configuration.c_conditional.bean.Boss;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author spectred
 */
@Configuration
//@Profile("city")
public class BartenderConfiguration {

    //    @ConditionalOnBean(beanNames = "org.spectred.doraemon.spring.configuration.c_conditional.bean.Boss")
    @ConditionalOnBean(Boss.class)
    @Bean
    public Bartender bartenderA() {
        Bartender bartender = new Bartender();
        bartender.setName("A");
        return bartender;
    }

    @Bean
    public Bartender bartenderB() {
        Bartender bartender = new Bartender();
        bartender.setName("B");
        return bartender;
    }
}
