package org.spectred.doraemon.spring.configuration.a_module.config;

import org.spectred.doraemon.spring.configuration.a_module.bean.Bartender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class BartenderConfiguration {

    @Bean
    public Bartender bartenderA() {
        Bartender bartender = new Bartender();
        bartender.setName("A");
        return bartender;
    }

    @Bean
    public Bartender bartenderB() {
        Bartender bartender = new Bartender();
        bartender.setName("B");
        return bartender;
    }
}
