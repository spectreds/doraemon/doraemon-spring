package org.spectred.doraemon.spring.configuration.b_profile.config;

import org.spectred.doraemon.spring.configuration.b_profile.annotation.EnableTavern;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@EnableTavern
@Configuration
public class TavernConfiguration {
}
