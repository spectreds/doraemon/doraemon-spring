package org.spectred.doraemon.spring.configuration.b_profile.bean;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Bartender {

    private String name;
}
