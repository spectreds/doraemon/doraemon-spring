package org.spectred.doraemon.spring.configuration.c_conditional.config;

import org.spectred.doraemon.spring.configuration.c_conditional.annotation.EnableTavern;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@EnableTavern
@Configuration
public class TavernConfiguration {
}
