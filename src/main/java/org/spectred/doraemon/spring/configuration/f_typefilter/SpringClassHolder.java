package org.spectred.doraemon.spring.configuration.f_typefilter;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author spectred
 */
@Component
public class SpringClassHolder implements ApplicationContextAware, DisposableBean {

    private static ApplicationContext ctx;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (Objects.isNull(ctx)) {
            ctx = applicationContext;
        }
    }

    @Override
    public void destroy() throws Exception {
        ctx = null;
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        return ctx.getBean(name,clazz);
    }

}
