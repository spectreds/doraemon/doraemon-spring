package org.spectred.doraemon.spring.configuration.c_conditional.bean;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Bartender {

    private String name;
}
