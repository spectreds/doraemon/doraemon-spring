package org.spectred.doraemon.spring.configuration.c_conditional.selector;

import org.spectred.doraemon.spring.configuration.c_conditional.bean.Bar;
import org.spectred.doraemon.spring.configuration.c_conditional.config.BarConfiguration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author spectred
 */
public class BarImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata metadata) {
        return new String[]{Bar.class.getName(), BarConfiguration.class.getName()};
    }
}
