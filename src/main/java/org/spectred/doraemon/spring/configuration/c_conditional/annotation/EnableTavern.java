package org.spectred.doraemon.spring.configuration.c_conditional.annotation;

import org.spectred.doraemon.spring.configuration.c_conditional.bean.Boss;
import org.spectred.doraemon.spring.configuration.c_conditional.config.BartenderConfiguration;
import org.spectred.doraemon.spring.configuration.c_conditional.registrar.WaiterRegistrar;
import org.spectred.doraemon.spring.configuration.c_conditional.selector.BarImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({/*Boss.class, */BartenderConfiguration.class, BarImportSelector.class, WaiterRegistrar.class})
public @interface EnableTavern {


}
