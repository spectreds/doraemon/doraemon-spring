package org.spectred.doraemon.spring.configuration.f_typefilter.zoo;

import org.spectred.doraemon.spring.configuration.f_typefilter.annotation.Animal;
import org.springframework.stereotype.Component;

/**
 * @author spectred
 */
@Animal
@Component
public class Dog {
}
