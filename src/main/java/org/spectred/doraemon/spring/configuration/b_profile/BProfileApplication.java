package org.spectred.doraemon.spring.configuration.b_profile;

import org.spectred.doraemon.spring.configuration.b_profile.config.TavernConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * @author spectred
 */
public class BProfileApplication {

    public static void main(String[] args){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("city");
        context.register(TavernConfiguration.class);
        context.refresh();

        Stream.of( context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
