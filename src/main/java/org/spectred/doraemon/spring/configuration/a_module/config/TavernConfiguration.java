package org.spectred.doraemon.spring.configuration.a_module.config;

import org.spectred.doraemon.spring.configuration.a_module.annotation.EnableTavern;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@EnableTavern
@Configuration
public class TavernConfiguration {
}
