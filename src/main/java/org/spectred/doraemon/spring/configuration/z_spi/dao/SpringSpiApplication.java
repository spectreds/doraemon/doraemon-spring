package org.spectred.doraemon.spring.configuration.z_spi.dao;

import org.springframework.core.io.support.SpringFactoriesLoader;

import java.util.List;

/**
 * @author spectred
 */
public class SpringSpiApplication {

    public static void main(String[] args) {

        // 加载并实例化
        List<DemoDao> list = SpringFactoriesLoader.loadFactories(DemoDao.class, SpringSpiApplication.class.getClassLoader());
        list.forEach(System.out::println);

        // 只加载全限定类名
        List<String> strings = SpringFactoriesLoader.loadFactoryNames(DemoDao.class, SpringSpiApplication.class.getClassLoader());
        strings.forEach(System.out::println);
    }
}
