package org.spectred.doraemon.spring.configuration.z_spi.dao;

import java.util.ServiceLoader;

/**
 * @author spectred
 */
public class JdkSpiApplication {

    public static void main(String[] args){
        ServiceLoader<DemoDao> serviceLoader = ServiceLoader.load(DemoDao.class);
        serviceLoader.forEach(System.out::println);
    }
}
