package org.spectred.doraemon.spring.configuration.b_profile.selector;

import org.spectred.doraemon.spring.configuration.b_profile.bean.Bar;
import org.spectred.doraemon.spring.configuration.b_profile.config.BarConfiguration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author spectred
 */
public class BarImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata metadata) {
        return new String[]{Bar.class.getName(), BarConfiguration.class.getName()};
    }
}
