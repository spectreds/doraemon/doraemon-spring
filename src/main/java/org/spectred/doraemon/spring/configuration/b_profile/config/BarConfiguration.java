package org.spectred.doraemon.spring.configuration.b_profile.config;

import org.spectred.doraemon.spring.configuration.b_profile.bean.Bar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
public class BarConfiguration {

    @Bean
    public Bar bar(){
        return new Bar();
    }
}
