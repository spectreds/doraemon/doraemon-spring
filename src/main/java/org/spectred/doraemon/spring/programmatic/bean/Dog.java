package org.spectred.doraemon.spring.programmatic.bean;

/**
 * @author spectred
 */
public class Dog extends Animal {

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", person=" + person +
                '}';
    }
}
