package org.spectred.doraemon.spring.programmatic.bean;

/**
 * @author spectred
 */
public class Cat extends Animal {

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", person=" + person +
                '}';
    }
}
