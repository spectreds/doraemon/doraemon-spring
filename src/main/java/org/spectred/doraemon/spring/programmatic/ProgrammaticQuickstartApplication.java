package org.spectred.doraemon.spring.programmatic;

import org.spectred.doraemon.spring.programmatic.bean.Cat;
import org.spectred.doraemon.spring.programmatic.bean.Dog;
import org.spectred.doraemon.spring.programmatic.bean.Person;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class ProgrammaticQuickstartApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        BeanDefinition personDefinition = BeanDefinitionBuilder.rootBeanDefinition(Person.class)
                .addPropertyValue("name", "Jack")
                .getBeanDefinition();
        context.registerBeanDefinition("jack", personDefinition);


        BeanDefinition catDefinition = BeanDefinitionBuilder.rootBeanDefinition(Cat.class)
                .addPropertyValue("name", "mimi")
                .addPropertyReference("person", "jack")
                .getBeanDefinition();
        context.registerBeanDefinition("mimi", catDefinition);

        BeanDefinition dogDefinition = BeanDefinitionBuilder.rootBeanDefinition(Dog.class)
                .addPropertyValue("name", "wangwang")
                .addPropertyReference("person", "jack")
                .setScope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
                .setLazyInit(true)
                .getBeanDefinition();
        context.registerBeanDefinition("wangwang", dogDefinition);

        context.refresh();

        Person person = context.getBean(Person.class);
        System.out.println(person);

        Cat cat = context.getBean(Cat.class);
        System.out.println(cat);

        Dog dog = context.getBean(Dog.class);
        System.out.println(dog);

    }
}
