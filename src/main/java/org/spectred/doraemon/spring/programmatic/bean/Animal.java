package org.spectred.doraemon.spring.programmatic.bean;

/**
 * @author spectred
 */
public abstract class Animal {

    protected String name;

    protected Person person;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", person=" + person +
                '}';
    }
}
