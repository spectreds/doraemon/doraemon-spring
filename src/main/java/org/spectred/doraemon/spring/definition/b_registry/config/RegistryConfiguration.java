package org.spectred.doraemon.spring.definition.b_registry.config;

import org.spectred.doraemon.spring.definition.b_registry.registry.PersonRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author spectred
 */
@Configuration
@Import(PersonRegistry.class)
public class RegistryConfiguration {
}
