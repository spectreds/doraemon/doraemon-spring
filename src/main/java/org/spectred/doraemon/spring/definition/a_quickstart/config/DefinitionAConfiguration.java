package org.spectred.doraemon.spring.definition.a_quickstart.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author spectred
 */
@Configuration
@ComponentScan(value = "org.spectred.doraemon.spring.definition.a_quickstart.bean")
public class DefinitionAConfiguration {
}
