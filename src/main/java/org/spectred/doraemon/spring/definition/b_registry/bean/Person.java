package org.spectred.doraemon.spring.definition.b_registry.bean;

import lombok.Data;

/**
 * @author spectred
 */
@Data
public class Person {

    private String name;
}
