package org.spectred.doraemon.spring.definition.c_removedefinition;

import org.spectred.doraemon.spring.definition.c_removedefinition.bean.Person;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author spectred
 */
public class RemoveDefinitionApplication {

    public static void main(String[] args){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("definition/definition.xml");
        Person jack = (Person)context.getBean("jack");
        System.out.println(jack);

        Person mary = (Person)context.getBean("mary");
        System.out.println(mary);
    }
}
