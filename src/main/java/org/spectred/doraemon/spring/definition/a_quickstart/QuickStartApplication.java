package org.spectred.doraemon.spring.definition.a_quickstart;

import org.spectred.doraemon.spring.definition.a_quickstart.config.DefinitionAConfiguration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class QuickStartApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DefinitionAConfiguration.class);
        BeanDefinition cat = context.getBeanFactory().getBeanDefinition("cat");
        System.out.println(cat.toString());
    }
}
