package org.spectred.doraemon.spring.definition.b_registry;

import org.spectred.doraemon.spring.definition.b_registry.bean.Person;
import org.spectred.doraemon.spring.definition.b_registry.config.RegistryConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author spectred
 */
public class RegistryApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
                RegistryConfiguration.class);
        Person person = ctx.getBean(Person.class);
        System.out.println(person);
    }
}
